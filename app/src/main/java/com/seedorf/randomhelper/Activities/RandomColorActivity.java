package com.seedorf.randomhelper.Activities;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.seedorf.randomhelper.R;

import java.util.Random;

import static com.seedorf.randomhelper.Helpers.DataHelper.copyDataToClipboard;

public class RandomColorActivity extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_color);
        setTitle(R.string.title_random_color);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // initAd();

        final TextView tvRGB = findViewById(R.id.tv_random_color_rgb);
        final TextView tvHEX = findViewById(R.id.tv_random_color_hex);
        final Button btnMakeRandom = findViewById(R.id.btn_random);
        final RelativeLayout colorLayout = findViewById(R.id.layout_color);

        /*AdView mAdview = findViewById(R.id.adViewColor);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdview.loadAd(adRequest);*/

        btnMakeRandom.setOnClickListener(new View.OnClickListener() { // TODO: standalone method
            @Override
            public void onClick(View view) {
                Random rnd = new Random();
                int r = rnd.nextInt(255);
                int g = rnd.nextInt(255);
                int b = rnd.nextInt(255);
                String resultRGB = "R: " + String.valueOf(r)
                        + " G: " + String.valueOf(g)
                        + " B: " + String.valueOf(b);
                String resultHex = String.format("#%02x%02x%02x", r, g, b);
                tvHEX.setText(resultHex);
                tvRGB.setText(resultRGB);
                colorLayout.setBackgroundColor(Color.rgb(r, g, b));
            }
        });

        tvHEX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyDataToClipboard(v.getContext(), ((TextView) v).getText().toString());
            }
        });

        tvRGB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyDataToClipboard(v.getContext(), ((TextView) v).getText().toString());
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void initAd() {
        /*
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.ad_interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build()); */
    }

    @Override
    public void onBackPressed() {
        /* if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } */
        super.onBackPressed();
    }
}
