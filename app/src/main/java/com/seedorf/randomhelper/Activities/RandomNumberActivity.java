package com.seedorf.randomhelper.Activities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.seedorf.randomhelper.Helpers.ViewHelper;
import com.seedorf.randomhelper.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static com.seedorf.randomhelper.Helpers.DataHelper.copyDataToClipboard;

public class RandomNumberActivity extends AppCompatActivity {
    public static final String RANDOM_NUMBER_PREFERENCES = "randomnumberpreferences";
    private static final String PREFERENCES_NUMB_COUNT = "Count";
    private static final String PREFERENCES_START = "Start";
    private static final String PREFERENCES_END = "End";
    SharedPreferences mSettings;

    private TextView tvResult;
    private EditText etNumCount;
    private EditText etStartNumBorder;
    private EditText etEndNumBorder;
    private CheckBox cbAllowRepeatNumber;

    Context mContext;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_number);
        setTitle(R.string.title_random_number);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mContext = this;
        tvResult = findViewById(R.id.tv_random_number);
        etNumCount = findViewById(R.id.et_how_much_numbers);
        etStartNumBorder = findViewById(R.id.et_start_border);
        etEndNumBorder = findViewById(R.id.et_end_border);
        cbAllowRepeatNumber = findViewById(R.id.cb_allow_repeat_numbers);

        mSettings = getSharedPreferences(RANDOM_NUMBER_PREFERENCES, MODE_PRIVATE);
        if (mSettings.contains(PREFERENCES_NUMB_COUNT)) {
            etNumCount.setText(String.valueOf(mSettings.getInt(PREFERENCES_NUMB_COUNT, 1)));
        }
        if (mSettings.contains(PREFERENCES_START)) {
            etStartNumBorder.setText(String.valueOf(mSettings.getInt(PREFERENCES_START, 0)));
        }
        if (mSettings.contains(PREFERENCES_END)) {
            etEndNumBorder.setText(String.valueOf(mSettings.getInt(PREFERENCES_END, 10)));
        }

        final Button btnMakeRandom = findViewById(R.id.btn_random);
        btnMakeRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { //TODO STANDALONE METHOD
                ViewHelper.hideKeyboard(mContext);
                tvResult.setVisibility(View.VISIBLE);
                if (validateFields()) {

                    StringBuilder stringBuilder = new StringBuilder();


                    if (!cbAllowRepeatNumber.isChecked()) { // not repeat
                        int numCount = Integer.valueOf(etNumCount.getText().toString());
                        int numStart = Integer.valueOf(etStartNumBorder.getText().toString());
                        int numEnd = Integer.valueOf(etEndNumBorder.getText().toString());
                        List<Integer> numsArray = new ArrayList<>();
                        for (int i = numStart; i <= numEnd; i++) {
                            numsArray.add(i);
                        }
                        Collections.shuffle(numsArray);
                        for (int i = 0; i < numCount; i++) {
                            stringBuilder.append(numsArray.get(i));
                            if (i != numCount - 1) {
                                stringBuilder.append(", ");
                            }
                        }

                    } else {
                        int numCount = Integer.valueOf(etNumCount.getText().toString());
                        Random rnd = new Random();

                        int numStart = Integer.valueOf(etStartNumBorder.getText().toString());
                        int numEnd = Integer.valueOf(etEndNumBorder.getText().toString());
                        int numRange = numEnd - numStart + 1;
                        for (int i = 0; i < numCount; i++) {
                            int rndNum = rnd.nextInt(numRange) + numStart;
                            stringBuilder.append(String.valueOf(rndNum));
                            if (i != numCount - 1) {
                                stringBuilder.append(", ");
                            }
                        }
                    }
                    tvResult.setText(stringBuilder.toString());
                }
            }
        });
        tvResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyDataToClipboard(v.getContext(), ((TextView) v).getText().toString());
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt(PREFERENCES_NUMB_COUNT, Integer.parseInt(etNumCount.getText().toString()));
        editor.putInt(PREFERENCES_START,  Integer.parseInt(etStartNumBorder.getText().toString()));
        editor.putInt(PREFERENCES_END,  Integer.parseInt(etEndNumBorder.getText().toString()));
        editor.apply();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private boolean validateFields() {
        final EditText etNumCount = findViewById(R.id.et_how_much_numbers);
        final EditText etStartNumBorder = findViewById(R.id.et_start_border);
        final EditText etEndNumBorder = findViewById(R.id.et_end_border);
        final CheckBox cbRepetitions = findViewById(R.id.cb_allow_repeat_numbers);

        final String numCount = etNumCount.getText().toString();
        final String startNum = etStartNumBorder.getText().toString();
        final String endNum = etEndNumBorder.getText().toString();


        // TODO: REGEXP
        if (numCount.equals("")) {
            etNumCount.setError(getResources().getString(R.string.error_empty_field));
            return false;
        }
        if (startNum.equals("") || startNum.equals("-") || startNum.equals("+")) {
            etStartNumBorder.setError(getResources().getString(R.string.error_empty_field));
            return false;
        }
        if (endNum.equals("") || endNum.equals("-") || endNum.equals("+")) {
            etEndNumBorder.setError(getResources().getString(R.string.error_empty_field));
            return false;
        }

        int numStart = Integer.valueOf(etStartNumBorder.getText().toString());
        int numEnd = Integer.valueOf(etEndNumBorder.getText().toString());
        if (numStart > numEnd) {
            etEndNumBorder.setError(getResources().getString(R.string.error_start_num_greater));
            return false;
        }

        if (!cbRepetitions.isChecked()) {
            int rangeCount = Integer.valueOf(etEndNumBorder.getText().toString())
                    - Integer.valueOf(etStartNumBorder.getText().toString()) + 1;

            if (rangeCount < Integer.valueOf(etNumCount.getText().toString())) {
                Toast.makeText(mContext, getString(R.string.error_impossible_generate_unique_numbers), Toast.LENGTH_SHORT).show();
                return false;
            }

        }
        return true;
    }

    private void initAd() { /*
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.ad_interstitial_test));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());*/
    }

    @Override
    public void onBackPressed() {
        // may be later...
       /* if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }*/
        super.onBackPressed();
    }
}
