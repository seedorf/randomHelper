package com.seedorf.randomhelper.Activities;

import android.animation.ObjectAnimator;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.seedorf.randomhelper.R;

import java.util.Random;

import static com.seedorf.randomhelper.Helpers.DataHelper.copyDataToClipboard;

public class CoinActivity extends AppCompatActivity {
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin);
        setTitle(R.string.title_coin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // initAd();

        final TextView tvCoin = findViewById(R.id.tv_coin);
        tvCoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyDataToClipboard(v.getContext(), ((TextView) v).getText().toString());
            }
        });


        Button btnToss = findViewById(R.id.btn_toss);
        btnToss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAnimation();
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        /*if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }*/
        super.onBackPressed();
    }

    private void startAnimation() {
        Random rnd = new Random();
        final TextView tvCoin = findViewById(R.id.tv_coin);
        final ImageView imgCoin = findViewById(R.id.imgCoin);

        if (rnd.nextBoolean()) {
            tvCoin.setText(R.string.eagle);
            imgCoin.setImageResource(R.drawable.coin_o);
        } else {
            tvCoin.setText(R.string.tails);
            imgCoin.setImageResource(R.drawable.coin_r);
        }

        imgCoin.setRotationX(270);

        ObjectAnimator imgAnimator = ObjectAnimator
                .ofFloat(imgCoin, "rotationX", imgCoin.getRotationX(),
                        imgCoin.getRotationX() + 90).setDuration(300);

        imgAnimator.start();
    }

    private void initAd() {
        /*
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.ad_interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build()); */
    }
}

