package com.seedorf.randomhelper.Activities;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.seedorf.randomhelper.Classes.CountryGenerator;
import com.seedorf.randomhelper.Helpers.DataHelper;
import com.seedorf.randomhelper.R;

import java.util.Arrays;
import java.util.List;

public class CountryActivity extends AppCompatActivity {

    private Context mContext;
    private boolean firstCountry = true;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        setTitle(R.string.title_random_country);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mContext = this;

        // initAd();

        //createSpinner(); TODO:: NEXT RELEASE

        final Button btnGenerate = findViewById(R.id.btn_random_country);
        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateCountry();
            }
        });

        final TextView tvCountryResult = findViewById(R.id.tv_country_result);
        tvCountryResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataHelper.copyDataToClipboard(mContext, tvCountryResult.getText().toString());
            }
        });

        final TextView tvCountryHistory = findViewById(R.id.tv_country_history);
        tvCountryHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataHelper.copyDataToClipboard(mContext, tvCountryHistory.getText().toString());
            }
        });

    }

    private void generateCountry() {
        // Spinner spinnerContinent = findViewById(R.id.spinner_country_continent);
        // String continent = spinnerContinent.getSelectedItem().toString();
        TextView tvCountryResult = findViewById(R.id.tv_country_result);
        TextView tvCountryHistory = findViewById(R.id.tv_country_history);

        if (firstCountry) {
            firstCountry = false;
        } else {
            tvCountryHistory.setText(getString(R.string.previous_country) + " ");
            tvCountryHistory.append(tvCountryResult.getText());
        }

        tvCountryResult.setText(CountryGenerator.getCountry(mContext));
        // tvCountryResult.setText(CountryGenerator.getCountry(mContext, continent));
        startAnimation();
    }

    /*
    private void createSpinner() {
        Spinner spinnerContinent = findViewById(R.id.spinner_country_continent);
        String[] continents = getResources().getStringArray(R.array.continents);
        List<String> continentsList = Arrays.asList(continents);
        ArrayAdapter<String> contListAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, continentsList);
        spinnerContinent.setAdapter(contListAdapter);
    } */

    private void startAnimation() {
        TextView tvCountry = findViewById(R.id.tv_country_result);
        ObjectAnimator tvAnimator = ObjectAnimator
                .ofFloat(tvCountry, "alpha", 0, 1 ).setDuration(600);
        tvAnimator.start();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initAd() {
        /*
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.ad_interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        */
    }

    @Override
    public void onBackPressed() {
        /*if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }*/
        super.onBackPressed();
    }
}
