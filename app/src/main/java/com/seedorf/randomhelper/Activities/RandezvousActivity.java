package com.seedorf.randomhelper.Activities;

import android.animation.ObjectAnimator;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.seedorf.randomhelper.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.seedorf.randomhelper.Helpers.DataHelper.copyDataToClipboard;

public class RandezvousActivity extends AppCompatActivity {
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_randezvous);

        setTitle(R.string.title_randezvous);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        /*
        AdView mAdView = findViewById(R.id.adViewRandezvous);
        AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.loadAd(adRequest);*/

        // initAd();



        TextView tvRandezvous = findViewById(R.id.tv_randezvous);
        tvRandezvous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyDataToClipboard(v.getContext(), ((TextView) v).getText().toString());
            }
        });

        Button btnGenerateRandezvous = findViewById(R.id.btn_random_randezvous);
        btnGenerateRandezvous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateRandezvous();
            }
        });
    }


    @Override
    public void onBackPressed() {
        /*if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }*/
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initAd() {
        /*
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.ad_interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build()); */
    }


    private void generateRandezvous() {

        String [] places = getResources().getStringArray(R.array.randezvour_places);

        Random random = new Random();
        int index = random.nextInt(places.length - 1);

        TextView tvResultRandezvous = findViewById(R.id.tv_randezvous);
        tvResultRandezvous.setText(places[index]);
        startAnimation();

    }

    private void startAnimation() {
        TextView tvRandezvous = findViewById(R.id.tv_randezvous);
        ObjectAnimator tvAnimator = ObjectAnimator
                .ofFloat(tvRandezvous,"alpha", 0, 1 ).setDuration(550);
        tvAnimator.start();
    }

}