package com.seedorf.randomhelper.Activities;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.seedorf.randomhelper.Classes.LetterGenerator;
import com.seedorf.randomhelper.Helpers.DataHelper;
import com.seedorf.randomhelper.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LettersActivity extends AppCompatActivity {
    private Context mContext;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_letters);

        setTitle(R.string.title_letters);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mContext = this;
        // initAd();
        createSpinner();

        Button btnGenerate = findViewById(R.id.btn_random_letter);
        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateLetter();
            }
        });

        final TextView tvResult = findViewById(R.id.tv_letter_result);
        tvResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataHelper.copyDataToClipboard(mContext, tvResult.getText().toString());
            }
        });
        final TextView tvHistory = findViewById(R.id.tv_letters_history);
        tvHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataHelper.copyDataToClipboard(mContext, tvHistory.getText().toString());
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void generateLetter() {
        // get random letter
        Spinner langSpinner = findViewById(R.id.spinner_letter_language);
        String language = langSpinner.getSelectedItem().toString();
        String letter = LetterGenerator.generateLetter(mContext, language);

        // check for first gen
        TextView tvLettersHistory = findViewById(R.id.tv_letters_history);
        TextView tvResult = findViewById(R.id.tv_letter_result);

        if (tvLettersHistory.getText().toString().length() > 350) {
            tvLettersHistory.setText("");
        }

        if (tvLettersHistory.getText().toString().isEmpty()) {
            tvLettersHistory.setText(getString(R.string.previous) + " ");
        }

        // set value
        tvLettersHistory.append(letter + ", ");
        tvResult.setText(letter);

        startAnimation();
    }

    private void createSpinner() {
        Spinner spinnerLanguage = findViewById(R.id.spinner_letter_language);
        String[] languages = getResources().getStringArray(R.array.languages);
        List<String> languagesList = Arrays.asList(languages);
        ArrayAdapter<String> langListAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, languagesList);
        spinnerLanguage.setAdapter(langListAdapter);
    }

    private void startAnimation() {
        TextView tvLetterResult = findViewById(R.id.tv_letter_result);
        tvLetterResult.setRotationX(90);

        ObjectAnimator letterAnimator = ObjectAnimator
                .ofFloat(tvLetterResult, "rotationX", tvLetterResult.getRotationX(),
                        tvLetterResult.getRotationX() - 90)
                .setDuration(350);
        letterAnimator.start();
    }

    private void initAd() {
        /*mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.ad_interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());*/
    }

    @Override
    public void onBackPressed() {
        /* if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }*/
        super.onBackPressed();
    }
}
