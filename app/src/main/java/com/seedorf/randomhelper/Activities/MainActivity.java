package com.seedorf.randomhelper.Activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.MobileAds;
import com.seedorf.randomhelper.Activities.AboutActivity;
import com.seedorf.randomhelper.Activities.CoinActivity;
import com.seedorf.randomhelper.Activities.PasswordActivity;
import com.seedorf.randomhelper.Activities.RandomColorActivity;
import com.seedorf.randomhelper.Activities.RandomNumberActivity;
import com.seedorf.randomhelper.Activities.YesNoActivity;
import com.seedorf.randomhelper.Classes.MenuItem;
import com.seedorf.randomhelper.Enums.ActivityClass;
import com.seedorf.randomhelper.Helpers.DataHelper;
import com.seedorf.randomhelper.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mMenuRecyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        setTitle(R.string.app_second_name);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        MobileAds.initialize(this, getString(R.string.admob_app_id));


        mMenuRecyclerView = findViewById(R.id.menu_recycler_view);
        mMenuRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        MenuAdapter mMenuAdapter = new MenuAdapter(DataHelper.getMenuItems(this));
        mMenuRecyclerView.setAdapter(mMenuAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_about, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class MenuHolder extends RecyclerView.ViewHolder {
        public TextView mTvTitle;
        private ImageView mImageItem;
        private ActivityClass mActivityClass;

        public MenuHolder(View view) {
            super(view);
            mTvTitle = itemView.findViewById(R.id.tv_menu_item_title);
            mImageItem = itemView.findViewById(R.id.img_menu_item);
        }

    }

    private class MenuAdapter extends RecyclerView.Adapter<MenuHolder> implements View.OnClickListener {
        private ArrayList<MenuItem> mItems;

        public MenuAdapter(ArrayList<MenuItem> items) {
            mItems = items;
        }

        @NonNull
        @Override
        public MenuHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view = LayoutInflater.from(getApplicationContext())
                    .inflate(R.layout.list_item_menu_main, viewGroup, false);

            view.setOnClickListener(this);

            return new MenuHolder(view);
        }

        @Override
        public void onClick(View view) {
            int itemPosition = mMenuRecyclerView.getChildLayoutPosition(view);
            ActivityClass menuClass = mItems.get(itemPosition).getActivityClass();
            startNewActivity(menuClass);
        }

        @Override
        public void onBindViewHolder(@NonNull MenuHolder menuHolder, int i) {
            MenuItem item = mItems.get(i);
            menuHolder.mTvTitle.setText(item.getTitle());
            menuHolder.mImageItem.setImageResource(item.getImgSrc());
        }


        @Override
        public int getItemCount() {
            return mItems.size();
        }

        private void startNewActivity(ActivityClass item) {
            Intent intent;
            switch (item) {

                case ACTIVITY_RANDOM_NUM:
                    intent = new Intent(getApplicationContext(), RandomNumberActivity.class);
                    break;
                case ACTIVITY_RANDOM_COLOR:
                    intent = new Intent(getApplicationContext(), RandomColorActivity.class);
                    break;
                case ACTIVITY_YES_NO:
                    intent = new Intent(getApplicationContext(), YesNoActivity.class);
                    break;
                case ACTIVITY_COIN:
                    intent = new Intent(getApplicationContext(), CoinActivity.class);
                    break;
                case ACTIVITY_DICE:
                    intent = new Intent(getApplicationContext(), DiceActivity.class);
                    break;
                case ACTIVITY_PASSWORD:
                    intent = new Intent(getApplicationContext(), PasswordActivity.class);
                    break;
                case ACTIVITY_LETTERS:
                    intent = new Intent(getApplicationContext(), LettersActivity.class);
                    break;
                case ACTIVITY_RANDEZVOUS:
                    intent = new Intent(getApplicationContext(), RandezvousActivity.class);
                    break;
                case ACTIVITY_COUNTRY:
                    intent = new Intent(getApplicationContext(), CountryActivity.class);
                    break;
                default:
                    intent = new Intent(getApplicationContext(), RandomNumberActivity.class);
            }
            startActivity(intent);
        }
    }
}


