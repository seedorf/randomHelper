package com.seedorf.randomhelper.Activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.seedorf.randomhelper.Classes.PasswordGenerator;
import com.seedorf.randomhelper.Helpers.ViewHelper;
import com.seedorf.randomhelper.R;

import static com.seedorf.randomhelper.Helpers.DataHelper.copyDataToClipboard;

public class PasswordActivity extends AppCompatActivity {
    Context mContext;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        setTitle(R.string.title_random_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mContext = this;

        // initAd();
        final TextView tvPassword = findViewById(R.id.tv_random_password);
        tvPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyDataToClipboard(v.getContext(), ((TextView) v).getText().toString());
            }
        });

        final Button btnGeneratePassword = findViewById(R.id.btn_random_password);
        btnGeneratePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPassword.setVisibility(View.VISIBLE);
                ViewHelper.hideKeyboard(mContext);
                if(validateFields()) {
                    generatePassword();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initAd() {
        /*
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.ad_interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        */
    }

    @Override
    public void onBackPressed() {
        /* if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } */
        super.onBackPressed();
    }



    private boolean validateFields() {
        GridLayout gridCheckBox = findViewById(R.id.grid_layout_check_box);
        EditText etNumOfChar = findViewById(R.id.et_num_of_char);

        if(etNumOfChar.getText().toString().equals("")) { //validate et num of char
            etNumOfChar.setError(getString(R.string.error_empty_field));
            return false;
        }

        boolean checked = false;  // validate check boxes
        for (int i = 0; i < gridCheckBox.getChildCount(); i++) {
            if( ((CheckBox) gridCheckBox.getChildAt(i)).isChecked() ) {
                checked = true;
                break;
            }
        }

        if (!checked) {
            Toast.makeText(this, getString(R.string.error_all_check_boxes_unchecked),
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void generatePassword() {

        CheckBox cbLower = findViewById(R.id.cb_low_case);
        CheckBox cbUpper = findViewById(R.id.cb_capital_letters);
        CheckBox cbDigits = findViewById(R.id.cb_numbers);
        CheckBox cbSpecial = findViewById(R.id.cb_spec_chars);
        EditText etCharCount = findViewById(R.id.et_num_of_char);

        PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
                .useLower(cbLower.isChecked())
                .useUpper(cbUpper.isChecked())
                .useDigits(cbDigits.isChecked())
                .useSpecial(cbSpecial.isChecked())
                .build();

        int charCount = Integer.parseInt(etCharCount.getText().toString());
        String password = passwordGenerator.generate(charCount);

        TextView tvPassword = findViewById(R.id.tv_random_password);
        tvPassword.setText(password);
    }
}
