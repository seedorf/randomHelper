package com.seedorf.randomhelper.Activities;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import com.seedorf.randomhelper.Adapters.ImageAdapter;
import com.seedorf.randomhelper.Helpers.DataHelper;
import com.seedorf.randomhelper.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DiceActivity extends AppCompatActivity {

    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dice);

        setTitle(R.string.title_roll_the_dice);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        createSpinner();

        TextView tvDiceSum = findViewById(R.id.tv_dice_result);
        tvDiceSum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataHelper.copyDataToClipboard(mContext, ((TextView) v).getText().toString());
            }
        });

        Button btnRollTheDice = findViewById(R.id.btn_roll_the_dice);
        btnRollTheDice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollTheDice();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void createSpinner() {
        Spinner spinnerDiceCount = findViewById(R.id.spinner_dice_count);
        List<Integer> diceListItems = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            diceListItems.add(i + 1);
        }
        ArrayAdapter<Integer> diceListAdapter = new ArrayAdapter<Integer>(this,
                R.layout.support_simple_spinner_dropdown_item, diceListItems);

        spinnerDiceCount.setAdapter(diceListAdapter);
    }

    private void rollTheDice() {
        Spinner diceSpinner = findViewById(R.id.spinner_dice_count);
        int diceCount = Integer.parseInt(diceSpinner.getSelectedItem().toString());
        Random random = new Random();
        int sum = 0;
        for (int i = 0; i < diceCount; i++) {
            sum += random.nextInt(6) + 1;
        }

        TextView tvDiceSum = findViewById(R.id.tv_dice_result);
        tvDiceSum.setVisibility(View.VISIBLE);
        tvDiceSum.setText(String.valueOf(sum));
        startAnimation();
    }

    private void startAnimation() {
        TextView tvDiceSum = findViewById(R.id.tv_dice_result);
        ObjectAnimator tvAnimator = ObjectAnimator
                .ofFloat(tvDiceSum,"alpha", 0, 1 ).setDuration(550);
        tvAnimator.start();
    }

}



