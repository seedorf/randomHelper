package com.seedorf.randomhelper.Activities;

import android.animation.ObjectAnimator;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.seedorf.randomhelper.R;

import java.util.Random;

import static com.seedorf.randomhelper.Helpers.DataHelper.copyDataToClipboard;

public class YesNoActivity extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yes_no);
        setTitle(R.string.title_yes_no);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /*AdView adView = findViewById(R.id.adViewYesNo);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest); */

        // initAd();

        Button btnYesNo = findViewById(R.id.btn_random_yes_no);
        ImageView mImg = findViewById(R.id.imgYesNo);
        final TextView tvYesNo = findViewById(R.id.tv_yes_no);
        tvYesNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyDataToClipboard(v.getContext(), ((TextView) v).getText().toString());
            }
        });
        btnYesNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random rnd = new Random();
                boolean res = rnd.nextBoolean();
                startAnimation(res);
                String answer = res ? getString(R.string.yes) : getString(R.string.no);
                tvYesNo.setText(answer);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void startAnimation(boolean rndRes) {
        ImageView mImg = findViewById(R.id.imgYesNo);
        TextView mTv = findViewById(R.id.tv_yes_no);

        mImg.setVisibility(View.VISIBLE);
        int rotation = rndRes ? 1080 : 1260;
        mImg.setRotation(0);
        mTv.setRotationX(270);
        ObjectAnimator heightAnimator = ObjectAnimator
                .ofFloat(mImg, "rotation", mImg.getRotation(), mImg.getRotation() + rotation)
                .setDuration(500);

        ObjectAnimator textViewAnimator = ObjectAnimator
                .ofFloat(mTv, "rotationX", mTv.getRotationX(), mTv.getRotationX() + 90)
                .setDuration(300);

        textViewAnimator.start();
        heightAnimator.start();
    }

    private void initAd() {
        /*
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.ad_interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        */
    }

    @Override
    public void onBackPressed() {
         /*if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } */
        super.onBackPressed();
    }
}
