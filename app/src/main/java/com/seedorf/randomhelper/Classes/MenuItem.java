package com.seedorf.randomhelper.Classes;

import com.seedorf.randomhelper.Enums.ActivityClass;

public class MenuItem {
    private ActivityClass mActivityClass;
    private String mTitle;
    private int mImgSrc;

    public MenuItem(ActivityClass cls, String title, int img) {
        mActivityClass = cls;
        mTitle = title;
        mImgSrc = img;
    }

    public String getTitle() {
        return mTitle;
    }

    public ActivityClass getActivityClass() {
        return mActivityClass;
    }

    public int getImgSrc() {
        return mImgSrc;
    }


}
