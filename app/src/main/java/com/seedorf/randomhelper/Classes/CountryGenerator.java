package com.seedorf.randomhelper.Classes;

import android.content.Context;

import com.seedorf.randomhelper.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class CountryGenerator {

    public static String getCountry(Context context) {
        return generateCountry(context, R.array.countries_array);
    }

    public static String getCountry(Context context, String continent) {

        // TODO : refactoring
        String[] allContinents = context.getResources().getStringArray(R.array.continents);
        int continentIndex = 0;

        for (int i = 0; i < allContinents.length; i++) {
            if (allContinents[i].equals(continent)) {
                continentIndex = i;
            }
        }

        List<Integer> countryArrayList = Arrays.asList(R.array.europe_countries,
                R.array.asia_countries, R.array.africa_countries, R.array.oceania_countries,
                R.array.north_america_countries, R.array.south_america_countries);

        String resultCountry = "";


        switch (continentIndex) {
            case 0 :  // all
                Random random = new Random();
                int contIndex = random.nextInt(countryArrayList.size()); // random continent
                resultCountry = generateCountry(context, countryArrayList.get(contIndex));
                break;
            case 1 : // europe
                resultCountry = generateCountry(context, R.array.europe_countries);
                break;
            case 2 : // asia
                resultCountry = generateCountry(context, R.array.asia_countries);
                break;
            case 3 : // Africa
                resultCountry = generateCountry(context, R.array.africa_countries);
                break;
            case 4 : // N America
                resultCountry = generateCountry(context, R.array.north_america_countries);
                break;
            case 5 : // S America
                resultCountry = generateCountry(context, R.array.south_america_countries);
                break;
            case 6 : // Australia and Oceania
                resultCountry = generateCountry(context, R.array.oceania_countries);
                break;

        }

        return resultCountry;
    }


    private static String generateCountry(Context context, int countryResId) {
        String[] countries = context.getResources().getStringArray(countryResId);
        Random random = new Random();
        int index = random.nextInt(countries.length);
        return countries[index];
    }

    private static String generateEurope(Context context) {
        int idss = R.array.europe_countries;
        String[] countries = context.getResources().getStringArray(idss);
        Random random = new Random();
        int index = random.nextInt(countries.length);
        return countries[index];
    }

    private static String generateAsia(Context context) {
        String[] countries = context.getResources().getStringArray(R.array.asia_countries);
        Random random = new Random();
        int index = random.nextInt(countries.length);
        return countries[index];
    }

    private static String generateOceania(Context context) {
        String[] countries = context.getResources().getStringArray(R.array.oceania_countries);
        Random random = new Random();
        int index = random.nextInt(countries.length);
        return countries[index];
    }
}
