package com.seedorf.randomhelper.Classes;

import android.content.Context;
import android.widget.Switch;

import com.seedorf.randomhelper.R;

import java.util.Random;

public class LetterGenerator {

    private final static String ALPHABET_ENGLISH = "";
    private final static String ALPHABET_FRENCH = "";
    private final static String ALPHABET_GERMAN = "";
    private final static String ALPHABET_INDONESIAN = "";
    private final static String ALPHABET_ITALIAN = "";
    private final static String ALPHABET_POLISH = "";
    private final static String ALPHABET_PORTUGUESE = "";
    private final static String ALPHABET_ROMANIAN = "";
    private final static String ALPHABET_RUSSIAN = "";
    private final static String ALPHABET_SPANISH = "";
    private final static String ALPHABET_TURKISH = "";


    private final static String[] ALPHABETS = {
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ", //ENGLISH
            "ABCDEFGHIJKLMNOPQRSTUVWXYZÉÀÈÙÂÊÎÔÛÇËÏÜŸ", //FRENCH
            "ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜẞ", //GERMAN
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ", //ITALIAN
            "ABCDEFGHIJKLMNOPRSTUVWYZŹŻŚÓŃŁĘĆĄ", //POLISH
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ", //PORTUGUESE
            "ABCDEFGHIJKLMNOPQRSTUVWXYZĂÂÎȘȚ", //ROMANIAN
            "АБВГДЕЁЖЗИКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ", //RUSSIA
            "ABCDEFGHIJKLMNOPQRSTUVWXYZÑ", //SPANISH
            "ABCDEFGHIJKLMNOPRSTUVXYZÇĞİÖŞÜ" //TURKISH
    };


    private LetterGenerator() {
    }

    public static String generateLetter(Context context, String language) {

        String[] allLanguages = context.getResources().getStringArray(R.array.languages);
        int langIndex = 0;
        for(int i = 0; i < allLanguages.length; i++) { // find index of language in string-array
            if(allLanguages[i].equals(language)) {
                langIndex = i;
            }

        }

        int alphabetSize = ALPHABETS[langIndex].length();

        // generate random letter
        Random random = new Random();
        int letterIndex = random.nextInt(alphabetSize);

        return String.valueOf(ALPHABETS[langIndex].charAt(letterIndex));

    }
}
