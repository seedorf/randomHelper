package com.seedorf.randomhelper.Helpers;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.Menu;
import android.widget.Toast;

import com.seedorf.randomhelper.Classes.MenuItem;
import com.seedorf.randomhelper.Enums.ActivityClass;
import com.seedorf.randomhelper.R;

import java.util.ArrayList;

public class DataHelper {
    public static ArrayList<MenuItem> getMenuItems(Context context) {
        ArrayList<MenuItem> menuList = new ArrayList<>();
        MenuItem number = new MenuItem(ActivityClass.ACTIVITY_RANDOM_NUM,
                context.getString(R.string.title_random_number) , R.drawable.numbers);
        MenuItem color = new MenuItem(ActivityClass.ACTIVITY_RANDOM_COLOR,
                context.getString(R.string.title_random_color), R.drawable.colors);
        MenuItem yesNo = new MenuItem(ActivityClass.ACTIVITY_YES_NO,
                context.getString(R.string.title_yes_no), R.drawable.faq);
        MenuItem tess = new MenuItem(ActivityClass.ACTIVITY_COIN,
                context.getString(R.string.title_coin), R.drawable.coin);
        MenuItem dice = new MenuItem(ActivityClass.ACTIVITY_DICE,
                context.getString(R.string.roll_the_dice), R.drawable.dices);
        MenuItem letters = new MenuItem(ActivityClass.ACTIVITY_LETTERS,
                context.getString(R.string.title_letters), R.drawable.letters);
        MenuItem passwrd = new MenuItem(ActivityClass.ACTIVITY_PASSWORD,
                context.getString(R.string.title_random_password), R.drawable.password);
        MenuItem randezvous = new MenuItem(ActivityClass.ACTIVITY_RANDEZVOUS,
                context.getString(R.string.title_randezvous), R.drawable.card);
        MenuItem country = new MenuItem(ActivityClass.ACTIVITY_COUNTRY,
                context.getString(R.string.title_random_country), R.drawable.globe) ;

        menuList.add(number);
        menuList.add(color);
        menuList.add(yesNo);
        menuList.add(tess);
        // menuList.add(dice);  TODO: next release
        menuList.add(letters);
        menuList.add(passwrd);
        menuList.add(randezvous);
        menuList.add(country);
        return menuList;
    }

    public static void copyDataToClipboard(Context context, String data) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("random-helper", data);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context, context.getString(R.string.toast_clipboard), Toast.LENGTH_SHORT).show();
    }
}
