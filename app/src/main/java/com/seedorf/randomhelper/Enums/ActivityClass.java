package com.seedorf.randomhelper.Enums;

public enum ActivityClass {
    ACTIVITY_RANDOM_NUM, ACTIVITY_RANDOM_COLOR, ACTIVITY_YES_NO, ACTIVITY_COIN,
    ACTIVITY_PASSWORD, ACTIVITY_DICE, ACTIVITY_LETTERS, ACTIVITY_RANDEZVOUS, ACTIVITY_COUNTRY
}
